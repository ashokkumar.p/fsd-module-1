## Express CRUD Operations
CRUD operations, which stand for `Create`, `Read`, `Update`, and `Delete`, are fundamental operations that are commonly used in data management. In the context of web development, CRUD operations are typically implemented using HTTP methods to interact with a database or other data storage mechanism.

To implement CRUD operations in Express, you would typically define routes for each operation using the appropriate HTTP method we have four basic methods 

#### 1. `get()`: we use get() method to retrieve data items.
#### 2. `post()`: we use post() method to create a new data item.
#### 3. `put()`: we use put() method to modify an existing data item.
#### 4. `delete()`: we use delete() method to remove data items.

# get() :-
get method is used to retrieve the data items.

To perform the CRUD operations we need to install express 

#### 1. Install Express:
```bash
npm install express
```
#### 2. Create a `Routes` folder 

#### 3. Create a `getData.js` file in the Routes folder
```js
// get() method 

const express = require("express");
const app = express();
const mongodb = require("mongodb");
let mongoclient = mongodb.MongoClient;
let fetch = express.Router().get('/', (req, res) => {
    mongoclient.connect('mongodb://localhost:27017/mydb', (err, db) => {
        if (err) {
            throw err
        }
        else {
            db.collection("employeeDetails").find({}).toArray((err,array)=>{
                if(err){
                    throw err
                }
                else{
                    res.send(array) 
                }
            })
        }
    })
})
module.exports = fetch;
```
#### 4. Create a `server.js` file in the Routes folder
```js
const express = require("express");
const app = express();
app.use(express.json())
app.use("/",require("./Routes/getData"));
app.listen(3500,(res,req)=>{
    console.log("Server is running at http://localhost:3500/")
})
```

# post() :-
post method is used to create a new data item.

#### 1. Create a `postData.js` file in the Routes folder
```js
// post() method

const express = require("express");
const app = express();
const mongodb = require("mongodb");
app.use(express.json())
let mongoclient = mongodb.MongoClient;
let insert = express.Router().post('/', (req, res) => {
    let employee ={
            "empId": 121,
            "empName": "Robin",
            "age": 26,
            "salary": 30000,
            "city": "Hyderabad",
            "mail": "robin@gmail.com",
            "password": 1234    
    }
    mongoclient.connect('mongodb://localhost:27017/mydb', (err, db) => {
        if (err) {
            throw err
        }
        else {
            db.collection("employeeDetails").insertOne(employee,(err,result)=>{
                if(err){
                    throw err
                }
                else{
                    res.send("Insertion compltetd.....!")
                }
            })
        }
    })
})
module.exports = insert;

```

#### 2. `server.js` file
```js
const express = require("express");
const app = express();
app.use(express.json());
app.use("/",require("./Routes/postData"));
app.listen(3500,(res,req)=>{
    console.log("Server is running at http://localhost:3500/");
```
# put() :-
 put() method is used to modify an existing data item.

 #### 1. Create a `putData.js` file in the Routes folder
 ```js
 // put() method

const express = require("express");
const app = express();
const mongodb = require("mongodb");
app.use(express.json())
let mongoclient = mongodb.MongoClient;
let update = express.Router().put('/', (req, res) => {
    const filter = { empId: 121 }; 
const info = {
  $set: {
    age: 27,
    salary: 35000,
    city: "Delhi",
    mail: "robin1@gmail.com",
    password: 5678
    
  }
};
  
    mongoclient.connect('mongodb://localhost:27017/mydb', (err, db) => {

        if (err) {
            throw err
        }
        else {
            db.collection("employeeDetails").updateOne(filter,info,(err,result)=>{
                if(err){
                    throw err
                }
                else{
                    res.send("update compltetd.....!")
                }
            })
        }
    })
})
module.exports = update;

 ```
 #### 2. `server.js` file
```js
const express = require("express");
const app = express();
app.use(express.json());
app.use("/",require("./Routes/putData"));
app.listen(3500,(res,req)=>{
    console.log("Server is running at http://localhost:3500/");
```
# delete() :- 
delete() method is used to  remove data items.

#### 1. Create a `deleteData.js` file in the Routes folder
```js
const express = require("express");
const app = express();
const mongo = require("mongodb");
app.use(express.json());
let monogclient = mongo.MongoClient;
let deleteData = express.Router().delete("/",(req, res)=>{
 monogclient.connect('mongodb://localhost:27017/mydb', (err,db)=>{
  let del = {"age":30};
          if(err){
            throw err
          }
          else{
            db.collection("students").deleteOne(del,(err,result)=>{
                if(err){
                    throw err
                }
                else{
                  //res.send(result)
                  if(result.deletedCount>0){
                   res.send("Deletion success.........")
                  }
                  else{
                     res.send(`the record ${JSON.stringify(del)} is not found`)
                  }
                  
                }
            })
          }
 })
});
module.exports = deleteData;
```
 #### 2. `server.js` file
```js
const express = require("express");
const app = express();
app.use(express.json());
app.use("/",require("./Routes/deleteData"));
app.listen(3500,(res,req)=>{
    console.log("Server is running at http://localhost:3500/");
```