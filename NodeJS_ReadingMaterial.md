# NodeJS Reading Material

## NODEJS'
Node.js is an open-source, server-side runtime environment built on Chrome's V8 JavaScript engine. It allows developers to run JavaScript code outside the browser, enabling server-side scripting and building scalable network applications.

Node.js introduced an event-driven, non-blocking I/O model, allowing JavaScript to handle server-side tasks efficiently, such as handling multiple concurrent connections without blocking.

- Node.js was created by Ryan Dahl, who introduced it in 2009.

### Installation:

Visit the Node.js official website (https://nodejs.org/) and download the installer for your operating system.

### Hello World:

Create a new file, e.g., hello.js.

Open it with a code editor and add the following code:

```js
console.log("Hello, Node.js!");
```
Open your terminal/command prompt, navigate to the directory containing `hello.js`, and run it with `node hello.js.`

### Modules:

Node.js has a module system that allows you to organize code. You can create your own modules or use built-in ones.
For example, to use the fs module for file operations, you can do:
```js
const fs = require('fs');
fs.readFile('file.txt', 'utf8', (err, data) => {
  if (err) throw err;
  console.log(data);
});
```
### NPM (Node Package Manager)

NPM is the default package manager for Node.js. 

It helps you install and manage libraries (packages) for your projects.

To initialize a new project, create a `package.json` file with `npm init`. 

Follow the prompts to set up your project.
Install packages with npm install <package-name>.

## Node.js HTTP Module
The Node.js http module is a core module that allows you to create HTTP servers and make HTTP requests. Here's an overview of the key aspects of the http module:
## Creating an HTTP Server:
#### 1. Importing the Module:
```js
const http = require('http');
```
#### 2. Creating a Server:
```js
const server = http.createServer((req, res) => {
  res.write('Hello World!'); //write a response to the client
  res.end(); //end the response
});

```
#### 3. Listening to a Port:
```js
server.listen(3000, () => {
  console.log(`Server is listening....`);
});
```
Run this script and access it in your web browser.

*Example*
```js
const http = require('http');

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Hello, Node.js Server!');
});

server.listen(3000, () => {
  console.log('Server is running on http://localhost:3000/');
});
``` 
Run this script and access it in your web browser.
### Reading the HTML file synchronously
```html
 <!-- demo.html -->
<html>
<body>
<h1>This is HTML Page Content</h1>
</body>
</html>
```

```js
var http = require("http");
const nodemon = require('nodemon');
const fs = require("fs");
const html = fs.readFileSync("demo.html","utf-8");
const server = http.createServer(function(req,res){
  res.end(html);
})
server.listen(8080);
console.log("server is running");
```
## Simple Routing Server
Create a demo.html file 
```html
<!-- demo.html -->
<html>
<body>
<h1>This is {{%text%}} Content</h1>
</body>
</html>
```
Now create an index.js file 
```js
// index.js 

var http = require("http");
const nodemon = require('nodemon');
const fs = require("fs");
const html = fs.readFileSync("demo.html","utf-8");

const server = http.createServer(function(req,res){
  let path = req.url;
    if(path === "/" || path.toLocaleLowerCase() === "/home"){
        res.end(html.replace("{{%text%}}","home page"))
    }else if(path === "/about" || path.toLocaleLowerCase() === "/about"){
        res.end(html.replace("{{%text%}}","about page"))
    }else if(path === "/contact" || path.toLocaleLowerCase() === "/contact"){
        res.end(html.replace("{{%text%}}","contact page"))
    }else{
        res.end("<h1>404 Error Page not</h1>")
    }
    // res.end();
});
  
server.listen(8080);
console.log("server is running");
```
The server logic checks the requested URL path and responds with different content based on the path. If the path is `"/"`, `"/home"`, `"/about"`, or `"/contact`," it replaces the placeholder` {{%text%}}` in the HTML template with corresponding content ("home page," "about page," "contact page"). If none of these paths match, a 404 error message is sent.

# Node.js File System Module

## Node.js as a File Server
The Node.js file system module allows you to work with the file system on your computer.

To include the File System module, use the require() method:

> var fs = require( 'fs' );

Common use for the File System module:

## 1. Read files :

The fs module provides a number of functions for reading data from files. The most common function is fs.readFile(), which takes the path to the file as an argument and returns a promise that resolves to the contents of the file.

Assume we have the following HTML file (located in the same folder as Node.js):

> demofile1.html

``` html
<html>
<body>
<h1>My Header</h1>
<p>My paragraph.</p>
</body>
</html>
```
### Asynchronous File Reading (readFile):
Create a Node.js file that reads the HTML file, and return the content:

*Example*

```js
var http = require('http');
var fs = require('fs');
http.createServer(function (req, res) {
  //Open a file on the server and return its content:
  fs.readFile('demofile1.html', function(error, data) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data);
    return res.end();
  });
}).listen(8080);
```  
In this example, readFile is used to read the contents of `"demofile1.html"` asynchronously. It takes a callback function that is called once the file reading is complete. If there is an error, it is passed as the first argument to the callback; otherwise, the data is passed as the second argument. 

### Synchronous File Reading (readFileSync):
```js
var http = require("http");
const nodemon = require('nodemon');
const fs = require("fs");
const html = fs.readFileSync("demo.html","utf-8");
const server = http.createServer(function(req,res){
  res.end(html);
})
server.listen(8080);
console.log("server is running");
```
Save the code above in a file called "demo_readfile.js", and initiate the file:

Initiate demo_readfile.js:
> C:\Users\Your Name>node demo_readfile.js

If you have followed the same steps on your computer, you will see the same result as the example: http://localhost:8080

## 2. Create files :

The fs module also provides functions for creating new files. The most common function is `fs.writeFile()`, which takes the path to the file and the data to write to the file as arguments.

#### *fs.appendFile() :*
The `fs.appendFile()` method appends specified content to a file. If the file does not exist, the file will be created:

*Example :*

> Create a new file using the `appendFile()` method:


```js
var fs = require('fs');

fs.appendFile('mynewfile1.txt', 'Hello content!', function (err) {
  if (err) throw err;
  console.log('Saved!');
});
```

#### *fs.open() :*
The fs.open() method takes a "flag" as the second argument, if the flag is "w" for "writing", the specified file is opened for writing. If the file does not exist, an empty file is created:

*Example :*

> Create a new, empty file using the `open()` method:

```js
var fs = require('fs');

fs.open('mynewfile2.txt', 'w', function (err, file) {
  if (err) throw err;
  console.log('Saved!');
});
```

#### *fs.writeFile() :*

The `fs.writeFile()` method replaces the specified file and content if it exists. If the file does not exist, a new file, containing the specified content, will be created:

*Example :*

> Create a new file using the `writeFile()` method:

```js
var fs = require('fs');

fs.writeFile('mynewfile3.txt', 'Hello content!', function (err) {
  if (err) throw err;
  console.log('Saved!');
});
```

## 3. Update Files
The File System module has methods for updating files:

* fs.appendFile()
* fs.writeFile()

The `fs.appendFile()` method appends the specified content at the end of the specified file:

*Example :*

> Append "This is my text." to the end of the file "mynewfile1.txt":

```js
var fs = require('fs');

fs.appendFile('mynewfile1.txt', ' This is my text.', function (err) {
  if (err) throw err;
  console.log('Updated!');
});
```

The `fs.writeFile()` method replaces the specified file and content:

*Example*

> Replace the content of the file "mynewfile3.txt":

```js
var fs = require('fs');

fs.writeFile('mynewfile3.txt', 'This is my text', function (err) {
  if (err) throw err;
  console.log('Replaced!');
});
```

## 4. Delete Files
To delete a file with the File System module,  use the `fs.unlink()` method.

The `fs.unlink()` method deletes the specified file:

*Example*

> Delete "mynewfile2.txt":


```js
var fs = require('fs');

fs.unlink('mynewfile2.txt', function (err) {
  if (err) throw err;
  console.log('File deleted!');
});
```

## 5. Rename Files

To rename a file with the File System module,  use the fs.rename() method.

The `fs.rename()` method renames the specified file:

*Example*

> Rename "mynewfile1.txt" to "myrenamedfile.txt":

```js
var fs = require('fs');

fs.rename('mynewfile1.txt', 'myrenamedfile.txt', function (err) {
  if (err) throw err;
  console.log('File Renamed!');
});
```